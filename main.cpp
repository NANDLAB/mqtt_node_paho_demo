#include <iostream>
#include "mqtt_node_paho.hpp"

int main()
{
    try {
        boost::asio::io_context ioc;
        mqtt_node_paho mqtt_node(ioc, "demo", "localhost");
        mqtt_node.start();
        std::cout << "main: Before running IO context." << std::endl;
        ioc.run();
        std::cout << "main: After running IO context." << std::endl;
    }
    catch (const std::exception & e) {
        std::cout << "Terminating due to exception " << e.what() << "." << std::endl;
        return 1;
    }
    return 0;
}
